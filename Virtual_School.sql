USE [Virtual_School]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [nvarchar](500) NULL,
	[Correo] [nvarchar](500) NULL,
	[Contraseña] [nvarchar](500) NULL,
	[VerfContraseña] [nvarchar](500) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comentarios]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comentarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CursoId] [int] NULL,
	[Comentario] [nvarchar](500) NULL,
	[UsuarioId] [int] NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_Comentarios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ComentTema]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComentTema](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdTemaContent] [int] NULL,
	[IdUser] [int] NULL,
	[ComentTema] [nvarchar](500) NULL,
	[ReplyComment] [nvarchar](500) NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_ComentTema] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContenidoCurso]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContenidoCurso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[Aprender] [nvarchar](4000) NULL,
	[Contenido] [nvarchar](4000) NULL,
	[Requisitos] [nvarchar](4000) NULL,
	[Descripción] [nvarchar](4000) NULL,
	[ParaQuienEs] [nvarchar](4000) NULL,
 CONSTRAINT [PK_ContenidoCurso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Curso]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Curso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreProfesor] [nvarchar](500) NULL,
	[NombreCurso] [nvarchar](500) NULL,
	[Detalle] [nvarchar](500) NULL,
	[Caratula] [nvarchar](500) NULL,
	[Fecha] [datetime] NULL,
	[CantidadTemas] [int] NULL,
 CONSTRAINT [PK_Curso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Progreso]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Progreso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[IdUser] [int] NULL,
	[progress] [bit] NULL,
	[IdTemaCon] [int] NULL,
 CONSTRAINT [PK_Progreso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReplyComent]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReplyComent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ComentTId] [int] NULL,
	[UserId] [int] NULL,
	[Reply] [nvarchar](500) NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_ReplyComent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Seccion]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seccion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Secciones] [nvarchar](500) NULL,
 CONSTRAINT [PK_Seccion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TemaContenido]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemaContenido](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemaContent] [nvarchar](500) NULL,
	[IdSecciones] [int] NULL,
	[IdTemaSeccion] [int] NULL,
	[IdCurso] [int] NULL,
 CONSTRAINT [PK_TemaContenido] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Temas]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Texto] [nvarchar](4000) NULL,
	[Videos] [nvarchar](4000) NULL,
	[CursoId] [int] NULL,
	[IdTemaContent] [int] NULL,
 CONSTRAINT [PK_Videos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TemaSeccion]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemaSeccion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[IdSeccion] [int] NULL,
	[TemaSeccions] [nvarchar](500) NULL,
 CONSTRAINT [PK_TemaSección] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TomarCurso]    Script Date: 30/6/2021 19:32:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TomarCurso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NULL,
	[IdCurso] [int] NULL,
 CONSTRAINT [PK_TomarCurso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([Id], [Usuario], [Correo], [Contraseña], [VerfContraseña]) VALUES (1, N'admin', N'admin@gmail.com', N'IkiMQjOphXS0PT2Q0P8utf9Zfilj0U9wAOZ2b0mfvg19uBFaOWuFFoof5VxashxkYSZLR5IzfFCsa1AWPfjH/w==', N'IkiMQjOphXS0PT2Q0P8utf9Zfilj0U9wAOZ2b0mfvg19uBFaOWuFFoof5VxashxkYSZLR5IzfFCsa1AWPfjH/w==')
INSERT [dbo].[Account] ([Id], [Usuario], [Correo], [Contraseña], [VerfContraseña]) VALUES (2, N'Angel', N'angel_12@gmail.com', N'CxsY3LVBddDJGvfcdh0oupov7pAVTbhJDoouQGXll6Hqxev6Qd8B19CMYv2hHYQts0wsbR8rtxmqivftmfrbJw==', N'CxsY3LVBddDJGvfcdh0oupov7pAVTbhJDoouQGXll6Hqxev6Qd8B19CMYv2hHYQts0wsbR8rtxmqivftmfrbJw==')
INSERT [dbo].[Account] ([Id], [Usuario], [Correo], [Contraseña], [VerfContraseña]) VALUES (3, N'Luis', N'luis@gmail.com', N'IivTNlVrCqWktgd4Xf0/8xA7DbrqKR93VyDByQe2G4vfhqJ+yGUTzkmrs+MOmIZ2eigdpB1Wn5COCfFbWRopJw==', N'IivTNlVrCqWktgd4Xf0/8xA7DbrqKR93VyDByQe2G4vfhqJ+yGUTzkmrs+MOmIZ2eigdpB1Wn5COCfFbWRopJw==')
INSERT [dbo].[Account] ([Id], [Usuario], [Correo], [Contraseña], [VerfContraseña]) VALUES (4, N'Benjamin', N'benjamin@gmail.com', N'JyAi7rQ98anqd4ZUDdlvQM6MP7LvSXXtq6GKEwROmLTGPuuIhgu/7r8IGRT0LWWZNJvydEKsrrbQtR9eGLNQsw==', N'JyAi7rQ98anqd4ZUDdlvQM6MP7LvSXXtq6GKEwROmLTGPuuIhgu/7r8IGRT0LWWZNJvydEKsrrbQtR9eGLNQsw==')
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[ComentTema] ON 

INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (1, 29, 2, N'Buen tema', N'Hola      ', CAST(N'2021-06-10T20:16:36.177' AS DateTime))
INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (2, 27, 2, N'Buen', N'aawd', CAST(N'2021-06-16T23:30:36.697' AS DateTime))
INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (3, 0, 0, NULL, N'ffff      ', CAST(N'2021-06-16T23:32:14.720' AS DateTime))
INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (7, 27, 2, NULL, N'as        ', CAST(N'2021-06-16T23:21:49.730' AS DateTime))
INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (8, 28, 2, N'Me encanta buen tema', N'Buen tema', CAST(N'2021-06-16T23:43:24.387' AS DateTime))
INSERT [dbo].[ComentTema] ([Id], [IdTemaContent], [IdUser], [ComentTema], [ReplyComment], [Fecha]) VALUES (9, 49, 2, N'hola gente', N'hola que tal', CAST(N'2021-06-21T11:14:52.933' AS DateTime))
SET IDENTITY_INSERT [dbo].[ComentTema] OFF
GO
SET IDENTITY_INSERT [dbo].[ContenidoCurso] ON 

INSERT [dbo].[ContenidoCurso] ([Id], [IdCurso], [Aprender], [Contenido], [Requisitos], [Descripción], [ParaQuienEs]) VALUES (9, 9, N'● Este curso es para Freelancers, coaches, consultores or Startups que buscan captar nuevos clientes. ● Este curso NO es para MLMers, spammers or vendedores marketeros aficionados. ● Aprenderás las bases de la programación con las diferentes herramientas  ● Aprenderás un conocimiento basado en la experiencia académica universitaria  ● Aprenderás a diseñar tus propios diagramas de flujo en los mejores programas  ● Aprenderás a entender la lógica necesaria de los lenguajes de programación  ● Podrás migrar un diagrama a un lenguaje de programación  ● Plantear un algoritmo para la solución de un problema	', N' Sección 1: Metodología de la Programación  1.- Bienvenidos  2.- Metas  3.- Universidad  4.- ¿Qué es la programación?  5.- Ventajas de estudiar programación  6.- Lenguajes de programación  7.- Áreas del programador  8.- Examen  ● Sección 2: Diagramas de Flujo  9.- ¿Qué es un algoritmo?  10.- Características de los algoritmos  11.- ¿Qué es un diagrama de flujo?  12.- Símbolos utilizados en los diagramas de flujo  13.- Examen  ● Sección 3: Introducción a PSeInt  14.- ¿Qué es un pseudocódigo?  15.- ¿Qué es PSeInt?  16.- Ejercicio  17.- Variables y tipos de datos  18.- Leer o asignar valores  19.- Expresiones y operadores  20.- Operaciones  21.- Bucle o ciclos  22.- Sentencias o estructuras de control de flujo  23.- Diagrama de flujo en pseint  24.- Código del ejercicio  25.- Más ejercicios', N' ● Conocerá la sintaxis básica del lenguaje Java  ● Manejará el concepto de Variables y Operadores en Java  ● Estudiaremos la Programación Orientada a Objetos con Java  ● Aprenderá las Sentencias de Control y Ciclos en Java  ● Veremos el concepto de Funciones con Java  ● Estudiaremos el concepto de Herencia en Java  ● Aprenderemos a utilizar Arreglos en java  ● Manejaremos el concepto de Matrices en Java  ● Aprenderemos a Diseñar Clases en Java', N'Aquí hay algunas cosas que descubriremos en el curso:  ▪️Los mitos y errores más comunes del marketing en redes sociales que se enseñan como “mejores prácticas”.  ▪️Estudios de casos de éxito paso a paso sobre cómo se utilizó Facebook, los nichos en foros, y YouTube para adquirir más de 400 clientes de pago hasta la fecha.  ▪️Cómo utilizar grupos privados de Facebook para “presentar” su oferta de servicios sin ser tan insistente o molesto.  ▪️El marco de autoridad de AMOCh: un sistema de 4 pasos que le permite demostrar experiencia e invitar a la búsqueda de sus clientes.  ▪️Como usar publicaciones simples de Reddit para que los potenciales clientes le envíen correos electrónicos con preguntas sobre su servicio.  ▪️Los 4 componentes de un vídeo Sticky de YouTube, y como secuenciarlos en el orden más efectivo.  ▪️Como crear vídeos simples que atraigan a los clientes de sus sueños y hagan que ellos se comuniquen con usted.  ▪️“¡No necesitas miles de seguidores!”', N'El curso de Fundamentos de Java incluye los siguientes temas de estudio:')
INSERT [dbo].[ContenidoCurso] ([Id], [IdCurso], [Aprender], [Contenido], [Requisitos], [Descripción], [ParaQuienEs]) VALUES (10, 10, N'● Aprender a poner su dinero a trabajar mediante los bots automaticos de arbitraje cripto-fiat ● Conocimiento básico para comprender tanto el modelo FIAT como el modelo de las criptomonedas ● Saber de qué se trata el arbitraje así como en qué mercados se opera y ejemplo de operaciones de este tipo. ● Conocimientos de seguridad informática tanto para las billeteras de bitcoin como para diferentes comunicaciones ● Visión general : factores positivos, negativos, riesgos y oportunidades en este tipo de negocios.', N'● Sección 1: Conceptos básicos  1.- Introduction  2.- Ejemplo de creación de dinero y qué beneficios lleva el banco central y gobierno  3.- Visión general de modelo de Cripto moneda y Fiat dentro de la microeconomía  4.- Ejemplo comunicación entre bancs comerciales y banco central para transferencia  5.- Visión general wallet bitcoin, la base 58 y tipos de plataformas para el almacén  6.- Transacciones bitcoin en la Blockchain  7.- Exchanges online para comprar y vender Bitcoins ordenados por países en 2020  ● Sección 2: Seguridad  8.- Antiphishing  9.- Ejemplo de estafas más comunes a lo largo de la historia de internet  10.- Doble autenticación  11.- Ataques de denegación de servicio  ● Sección 3: Mind.Capital  12.- Sistema de Referidos en Mind.Capital  13.- Visión general de funcionamiento de operaciones de Mind.Capital y su fiscalidad', N'● Tienes que tener una cuenta bancaria o tarjeta para poder hacer depositos en Bitcoin. ● Tienes que estar en un país que no prohiba el uso del bitcoin. ● Acceso a internet para poder usar las diferentes plataformas de control, pago y monedero. ● Saber que toda inversión que se haga conforma un riesgo y no es recomendable invertir dinero que necesitas para vivir.', N'¿Le gustaría aprender la forma más rápida, fácil y detallada de entrar en el negocio de arbitraje cripto-fiat ? Si es así, te encantará este Curso Udemy, impartido por mí, LightningThought - Ingeniero Técnico informático de sistemas con un canal focalizado.  Este curso es la guía definitiva de arbitraje cripto-monedas y dinero FIAT mediante Mind.Capital y te enseñará todo lo que necesitas saber - entender Bitcoin, Blockchain, configurar una cartera segura, comprar Bitcoin, seguridad informática ( RSA, 2FA, etc) así como el funcionamiento del negocio de arbitraje como sus pros y contras.  El 2020 es posiblemente una de las últimas oportunidades de entrar en este negocio de cripto-monedas antes de que en unos años el arbitraje  cripto-fiat sea ajustado de tal forma que no haya hueco para sacar rendimientos positivos de forma fácil ( el arbitraje FIAT cuando nació permitió sacar beneficios rápidos haciendo cambio entre divisas hasta que fue ajustado ).  Este curso y los diferentes archivos excel que se incluye es una guía para su éxito personal y técnico/financiero en el campo del negocio cripto-fiat  Todas las herramientas que necesita para tener éxito con las cripto-divisas están incluidas en este curso y todo el curso está basado en el conocimiento práctico y la experiencia de la vida real y no en la teoría.  ¡Muchas gracias por su interés y cada día mejor!  Angel Serrano Perez  LightningThought', N'● Cualquiera que quiera aprender a invertir en cripto moneda y no sepa por dónde empezar ... ● Cualquiera que quiera aprender las estrategias de comercio e inversión del día que mantienen el dinero entrando diariamente ● ¡ Cualquiera que no entienda Blockchain y quiera aprender cómo funciona, simplemente !')
INSERT [dbo].[ContenidoCurso] ([Id], [IdCurso], [Aprender], [Contenido], [Requisitos], [Descripción], [ParaQuienEs]) VALUES (1011, 1011, N'● Aprenderás las bases de la programación con las diferentes herramientas

	● Aprenderás un conocimiento basado en la experiencia académica universitaria

	● Aprenderás a diseñar tus propios diagramas de flujo en los mejores programas

	● Aprenderás a entender la lógica necesaria de los lenguajes de programación

	● Podrás migrar un diagrama a un lenguaje de programación

	● Plantear un algoritmo para la solución de un problema', N'● Sección 1: Metodología de la Programación

 1.- Bienvenidos

 2.- Metas

 3.- Universidad

 4.- ¿Qué es la programación?

 5.- Ventajas de estudiar programación

 6.- Lenguajes de programación

 7.- Áreas del programador

 8.- Examen



● Sección 2: Diagramas de Flujo

 9.- ¿Qué es un algoritmo?

 10.- Características de los algoritmos

 11.- ¿Qué es un diagrama de flujo?

 12.- Símbolos utilizados en los diagramas de flujo

 13.- Examen



● Sección 3: Introducción a PSeInt

 14.- ¿Qué es un pseudocódigo?

 15.- ¿Qué es PSeInt?

 16.- Ejercicio

 17.- Variables y tipos de datos

 18.- Leer o asignar valores

 19.- Expresiones y operadores

 20.- Operaciones

 21.- Bucle o ciclos

 22.- Sentencias o estructuras de control de flujo

 23.- Diagrama de flujo en pseint

 24.- Código del ejercicio

 25.- Más ejercicios



● Sección 4: Introducción a DFD

 26.- ¿Qué es un diagrama de flujo de datos? (DFD)

 27.- Descargar

 28.- Asignar variables

 29.- Mostrar mensajes

 30.- Ciclos para

 31.- Realizar operaciones

 32.- Decisión

 33.- DFD del ejercicio



● Sección 5: Programación en C++

 34.- Migración

 35.- Código del Ejercicio

 36.- Recursos', N'● Se necesitan algunos programas como PSeInt, DFD y Code::Blocks

		● Conocimientos básicos en el manejo del sistema operativo windows

		● Ganas de aprender a diseñar diagramas de flujo

		● Algunos conocimientos previos de la programación no es necesario completamente pero tienes una mejor visión del tema', N'Dirigido para principiantes que quieren aprender a crear algoritmos en PSeInt para migrar su contenido a diagramas de flujo y después a un lenguaje de programación conociendo sus fundamentos previos y entender el manejo de los programas.



● Conocerás la metodología para resolver problemas usando la programación en un ambiente institucional educativo y laboral 

● Conceptos básicos de la programación

● Diseñaras tus primeros algoritmos en PSeInt

● Conoce el software que crea diagramas de flujo funcionales como lo es DFD



Temario Principal



1. Bienvenidos

2. Metas

3. Universidad

4. Ventajas de estudiar programación

5. Lenguajes de programación

6. ¿Qué es un algoritmo?

7. Caracteristicas de los algoritmos

8. ¿Qué es un diagrama de flujo?

9. Símbolos utilizados en los diagramas de flujo

10. ¿Qué es un pseudocódigo?

11. ¿Qué es PSeInt?



Definiciones



● Una variable es un espacio de la memoria donde guardar información

● La información que se guarda en la variable puede ser de diversos tipos y puede ir cambiando a lo largo del programa

● Cada variable tiene un tipo de dato asociado, por lo que siempre guardará el mismo tipo de dato

● Una variable que guarde un número no podrá guardar después otro tipo que no sea un número

● Las expresiones son combinaciones de constantes, variables y operadores que nos permiten trabajar con los datos

● Dependiendo de los operadores utilizados en ellas, pueden ser de varios tipos: aritméticas, relacionales, lógicas, alfanuméricas y de asignación', N'● Principiantes

● Técnicos

● Universitarios')
INSERT [dbo].[ContenidoCurso] ([Id], [IdCurso], [Aprender], [Contenido], [Requisitos], [Descripción], [ParaQuienEs]) VALUES (1012, 1012, N'● En este curso aprenderás C# con .net core desde cero hasta lo mas avanzado de este lenguaje de programacion

	● Aprenderemos a programar en el lenguaje de programacion de C# con POO

	● En en el trascurso del curso se irán creando aplicaciones con todo lo que se este aprendiendo en curso

	● Realizaremos conexiones de datos a los servidores de de MySQL y SQL Server

	● Al final de este curso tendrás los conocimiento básicos sobre el lenguaje de programacion de C# para desarrollar tu aplicaciones

	● El curso es principalmente para estudiantes que desean aprender C# desde cero', N'● Sección 1: Introducción al curso de C#

 1.- Aplicaciones que se pueden crear con el lenguaje de programación de C#

 2.- La ID Visual Studio y los entornos de desarrollo que se utilizan

 3.- Creando el proyecto en Visual Studio



● Sección 2: Tipos de variables en C#

 4.- Variables de tipo string, double, float, decimal, int

 5.- Variables de tipo bool, char y estructuras para almacenar datos



● Sección 3: Tipos de operadores en C#

 6.- Operadores aritméticos

 7.- Operadores de asignación

 8.- Operadores de comprobación de tipos y relaciones

 9.- Operadores de igualdad



● Sección 4: Estructura condicional en C#

 10.- Estructura if y los operadores condicionales

 11.- Estructura else que acompaña a la estructura if

 12.- Estructuras condicionales anidadas

 13.- El operador condicional



● Sección 5: Tipos de array o arreglos en C#

 14.- Array de tipo string

 15.- Array de tipo int

 16.- Arreglos Multidimensionales



● Sección 6: La estructura bucle ciclo For en C#

 17.- Bucle ciclo For

 18.- La estructura bucle ciclo Foreach



● Sección 7: Ejercicio Triángulo de Pascal en C#

 19.- Triángulo de Pascal



● Sección 8: Estructura condicional switch en C#

 20.- Instrucción de selección switch

 21.- Switch Expressions #1

 22.- Switch Expressions #2



● Sección 9: Las estructuras While and Do While en C#

 23.- La estructura While

 24.- La estructura do While



● Sección 10: Manipulación de String C#

 25.- Inmutabilidad de los objetos String

 26.- Formato de cadenas

 27.- Subcadenas de string

 28.- Iterando la cadena de texto



● Sección 11: La clase StringBuilder para la creación de cadenas en C#

 29.- La clase StringBuilder #1

 30.- La clase StringBuilder #2



● Sección 12: Tipos de métodos en C#

 31.- Modificador private y métodos que no retorna elementos

 32.- Modificador public y métodos que retorna un tipo de dato

 33.- Variables globales y método que retorna datos de tipo string

 34.- Método constructor de la clase

 35.- Leer Mayúsculas And Minúsculas

 36.- Sobrecarga de métodos constructores



● Sección 13: Parámetros de métodos

 37.- Cómo pasar un arreglo de tipo object como parámetro

 38.- Parámetros de métodos palabras clave params, in

 39.- Parámetros de métodos palabras clave ref, out



● Sección 14: Funciones en el lenguaje de programación en C#

 40.- Funciones

 41.- Funciones static

 42.- Datos duplicados en un array



● Sección 15: Palabra clave static en C#

 43.- Variables estáticas

 44.- Métodos estáticos

 45.- Clase estática

 46.- Conversor de velocidades



● Sección 16: Argumentos con nombre y opcionales en C#

 47.- Argumentos opcionales

 48.- Argumentos con nombres

 49.- Palabras claves readonly, const



● Sección 17: Propiedades en C#

 50.- Propiedades #1

 51.- Propiedades #2

 52.- Búsqueda de datos usando colecciones de objetos



● Sección 18: Clase genérica List en C#

 53.- Clase genérica List #1

 54.- Clase genérica List #2

 55.- Clase genérica List #3

 56.- Clase genérica List #4



● Sección 19: Aplicación para crear una lista enlazada

 57.- Cr', N'● Tener instalado el Visual Studio el más actual

		● No es necesario saber programar ya que en este curso se aprenderá a programar desde cero con C#', N'Hola mi nombre es Alex Pagoada y te invito a que seas parte de este curso donde aprenderemos sobre el lenguaje de programación de .net core con C# , lenguaje que nos permitirá crear diversas aplicaciones multiplataforma ,como aplicaciones de escritorio, aplicaciones web, y aplicaciones móviles y muchas mas aplicaciones , y te garantizo que al final de este curso tendrás los conocimientos básicos para crear tus aplicaciones usando este grandioso lenguaje de programación de C# , y espero que este curso sea de su agrado, y gracias por ser parte de este gran curso y también que Dios derrame muchas bendiciones en tu vida.', N'● Todo aquel quiera aprender a programar con el lenguaje de programación de C#')
SET IDENTITY_INSERT [dbo].[ContenidoCurso] OFF
GO
SET IDENTITY_INSERT [dbo].[Curso] ON 

INSERT [dbo].[Curso] ([Id], [NombreProfesor], [NombreCurso], [Detalle], [Caratula], [Fecha], [CantidadTemas]) VALUES (9, N'Ryan Cresswell, Brian Yang', N'Estrategias de marketing en redes sociales', N'Como utilizar los grupos de Facebook, los foros de nichos específicos, y vídeos de YouTube', N'https://www.guatemala.com/fotos/201705/marketing-p-885x500.jpg', CAST(N'2021-06-10T18:52:03.853' AS DateTime), 3)
INSERT [dbo].[Curso] ([Id], [NombreProfesor], [NombreCurso], [Detalle], [Caratula], [Fecha], [CantidadTemas]) VALUES (10, N'Angel Serrano Perez', N'Arbitraje cripto-monedas y dinero FIAT mediante Mind.Capital', N'Aprende Bitcoin, BlockChain, seguridad informática ( RSA, 2FA, etc ), negocio cripto-fiat (exchanges, bots, etc) en 2020', N'https://criptomonedaz.com/wp-content/uploads/2020/07/Arbitragem-Bitcoin-Comprar-Vender-2.jpg', CAST(N'2021-06-10T20:55:03.753' AS DateTime), 5)
INSERT [dbo].[Curso] ([Id], [NombreProfesor], [NombreCurso], [Detalle], [Caratula], [Fecha], [CantidadTemas]) VALUES (1011, N'Andrés Hernández Mata', N' Metodología de la Programación | PSeInt, DFC, C++', N'Crea algoritmos y diagramas de flujo para cualquier lenguaje de programación.', N'https://img-b.udemycdn.com/course/240x135/2971364_eb9b.jpg', CAST(N'2021-06-23T11:08:26.607' AS DateTime), 9)
INSERT [dbo].[Curso] ([Id], [NombreProfesor], [NombreCurso], [Detalle], [Caratula], [Fecha], [CantidadTemas]) VALUES (1012, N'Alex Joel Pagoada Suazo', N'Curso de C# .NET desde cero hasta lo mas avanzado full stack', N'Curso de C# .NET desde cero para el desarrollo de diversas aplicaciones multiplataforma', N'https://cdn-thumbs.comidoc.net/750/2310326_6691_6.jpg', CAST(N'2021-06-23T11:42:37.920' AS DateTime), 4)
SET IDENTITY_INSERT [dbo].[Curso] OFF
GO
SET IDENTITY_INSERT [dbo].[Progreso] ON 

INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1126, 1011, 2, 1, 1041)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1127, 1011, 2, 1, 1042)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1128, 1011, 2, 0, 1043)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1129, 1011, 2, 0, 1044)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1130, 1011, 2, 0, 1045)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1131, 1011, 2, 0, 1046)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1132, 1011, 2, 0, 1047)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1133, 1011, 2, 0, 1048)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1134, 1011, 2, 0, 1049)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1135, 1012, 2, 1, 1050)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1136, 1012, 2, 1, 1051)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1137, 1012, 2, 1, 1052)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1138, 1012, 2, 1, 1053)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1143, 10, 2, 0, 30)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1144, 10, 2, 0, 31)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1145, 10, 2, 0, 32)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1146, 10, 2, 0, 33)
INSERT [dbo].[Progreso] ([Id], [IdCurso], [IdUser], [progress], [IdTemaCon]) VALUES (1147, 10, 2, 0, 34)
SET IDENTITY_INSERT [dbo].[Progreso] OFF
GO
SET IDENTITY_INSERT [dbo].[ReplyComent] ON 

INSERT [dbo].[ReplyComent] ([Id], [ComentTId], [UserId], [Reply], [Fecha]) VALUES (1, 2, 2, N'Buenasa', CAST(N'2021-06-16T21:56:09.373' AS DateTime))
INSERT [dbo].[ReplyComent] ([Id], [ComentTId], [UserId], [Reply], [Fecha]) VALUES (2, 3, 2, N'hola ', CAST(N'2021-06-16T22:42:54.753' AS DateTime))
SET IDENTITY_INSERT [dbo].[ReplyComent] OFF
GO
SET IDENTITY_INSERT [dbo].[Seccion] ON 

INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (1, N'Seccion 1')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (2, N'Seccion 2')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (3, N'Seccion 3')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (4, N'Seccion 4')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (5, N'Seccion 5')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (6, N'Seccion 6')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (7, N'Seccion 7')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (8, N'Seccion 8')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (9, N'Seccion 9')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (10, N'Seccion 10')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (11, N'Seccion 11')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (12, N'Seccion 12')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (13, N'Seccion 13')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (14, N'Seccion 14')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (15, N'Seccion 15')
INSERT [dbo].[Seccion] ([Id], [Secciones]) VALUES (16, N'Seccion 16')
SET IDENTITY_INSERT [dbo].[Seccion] OFF
GO
SET IDENTITY_INSERT [dbo].[TemaContenido] ON 

INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (27, N'Bienvenidos', 1, 9, 9)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (28, N'Estrategia de Youtube', 1, 9, 9)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (29, N'Estructurar el contenido de forma educativa y atractiva', 1, 9, 9)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (30, N'Estructurar el contenido de forma educativa y atractiva', 1, 10, 10)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (31, N'Estrategia de Youtube', 1, 10, 10)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (32, N'Plataformas de conversación en línea y foros de nicho específicos (Reddit.com)', 2, 10, 10)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (33, N'Bienvenidos', 1, 10, 10)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (34, N'Bienvenidos', 3, 10, 10)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1041, N'Bienvenidos', 1, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1042, N'Metas', 1, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1043, N'Universidad', 1, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1044, N'Areas del programador', 2, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1045, N'¿Qué es un algoritmo?', 2, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1046, N'Características de los algoritmos', 2, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1047, N'¿Qué es un pseudocódigo?', 3, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1048, N'¿Qué es PseInt', 3, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1049, N'Variables y tipos de datos', 3, 1011, 1011)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1050, N'Aplicaciones que se pueden crear con el lenguaje de programacion de C#', 1, 1012, 1012)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1051, N'La ID Visual Studio y los entornos de desarrollo que se utilizan', 1, 1012, 1012)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1052, N'Variables de tipo  string,double,float,decimal,int', 2, 1012, 1012)
INSERT [dbo].[TemaContenido] ([Id], [TemaContent], [IdSecciones], [IdTemaSeccion], [IdCurso]) VALUES (1053, N'Variables de tipo bool,char y estructuras para almacenar datos', 2, 1012, 1012)
SET IDENTITY_INSERT [dbo].[TemaContenido] OFF
GO
SET IDENTITY_INSERT [dbo].[Temas] ON 

INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (28, N'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.', NULL, 9, 27)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (29, NULL, N'https://www.youtube.com/embed/8wAcvBJR_Ro', 9, 28)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (30, NULL, N'https://www.youtube.com/embed/8VnZlJW0HcA', 9, 29)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (31, NULL, N'https://www.youtube.com/embed/GZj9bHfrzsY', 10, 30)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (32, NULL, N'https://www.youtube.com/embed/Pa3ERUp5MzU', 10, 31)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (33, NULL, N'https://www.youtube.com/embed/8wAcvBJR_Ro', 10, 32)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (34, NULL, N'https://www.youtube.com/embed/8VnZlJW0HcA', 10, 33)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1042, NULL, N'https://www.youtube.com/embed/6Usdx1Ny1KE', 1011, 1041)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1043, N'La Programación de Metas o Programación por objetivos es un enfoque que permite abordar problemas de decisión general respecto a las metas que se deseen alcanzar en algún ámbito de la vida cotidiana. Estas metas pueden ser complementarias, pero frecuentemente conflictivas. Por ello, una forma de arreglar esta inconmensurabilidad se basa en una estructura prioritaria por parte de la administración.

La formulación de un modelo de Programación Meta es similar al modelo de P.L.. El Primer paso es definir las variables de decisión, después se deben de especificar todas las metas gerenciales en orden de prioridad. Así, una característica de la Programación Meta es que proporciona solución para los problemas de decisión que tengan metas múltiples, conflictivas e inconmensurables arregladas de acuerdo a la estructura prioritaria de la administración.', NULL, 1011, 1042)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1044, NULL, N'https://www.youtube.com/embed/QTDPOxnfRaI', 1011, 1044)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1045, NULL, N'https://www.youtube.com/embed/GFANLSu4F7o', 1011, 1043)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1046, NULL, N'https://www.youtube.com/embed/t5iAuuT5lSo', 1011, 1046)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1047, N'Un algoritmo informático es un conjunto de instrucciones definidas, ordenadas y acotadas para resolver un problema o realizar una tarea. 

En programación, supone el paso previo a ponerse a https://www.instagram.com/p/CGkHG_lg8xY/. Primero debemos encontrar la solución al problema (definir el algoritmo informático), para luego, a través del código, poder indicarle a la máquina qué acciones queremos que lleve a cabo. De este modo, un programa informático no sería más que un conjunto de algoritmos ordenados y codificados en un https://profile.es/blog/los-lenguajes-de-programacion-mas-queridos-y-mas-odiados/ para poder ser ejecutados en un ordenador.

No obstante, los algoritmos no son algo exclusivo de los ámbitos de las matemáticas, la lógica y la computación. Utilizamos numerosos algoritmos para resolver problemas en nuestra vida cotidiana. Algunos de los ejemplos más habituales son los manuales de instrucciones o las recetas de cocina. ', NULL, 1011, 1045)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1048, NULL, N'https://www.youtube.com/embed/FvibfpSVFBw', 1011, 1048)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1049, N'El pseudocódigo es una forma de expresar los distintos pasos que va a realizar un programa, de la forma más parecida a un lenguaje de programación. Su principal función es la de representar por pasos la solución a un problema o algoritmo, de la forma más detallada posible, utilizando un lenguaje cercano al de programación. El pseudocódigo no puede ejecutarse en un ordenador ya que entonces dejaría de ser pseudocódigo, como su propio nombre indica, se trata de un código falso (pseudo = falso), es un código escrito para que lo entienda el ser humano y no la máquina.', NULL, 1011, 1047)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1050, N'Un programa que solo muestre información en la pantalla no es más que una curiosidad que poca utilidad puede tener. En la vida real resulta necesario manipular información, de hecho es lo que regularmente hacen los programas. Tomar algún dato, procesarlo y presentarlo (ya sea en pantalla, enviarlo por algún medio, o simplemente entregarlo a otro proceso

Tomar un dato o conjunto de datos o realizar un proceso, implica «mover» información en uno o varios sentidos. Para realizar esta tarea utilizamos Variables.', NULL, 1011, 1049)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1051, NULL, N'https://www.youtube.com/embed/nxl89oJvMb4', 1012, 1050)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1052, NULL, N'https://www.youtube.com/embed/FUkKuz1QG_s', 1012, 1051)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1053, N'Los tipos de datos, como su nombre lo indica, especifican el tipo de valores que se pueden almacenar en una variable.
Algunos ejemplos de tipos de datos primitivos son enteros, de punto decimal flotante (fraccionarios), caracteres y boleanos.
Cuando se hace la declaración de variables, el tipo de dato le indica al compilador de que tipo será la variable. De esta manera se reserva la cantidad de memoria necesaria.
En la siguiente tabla se muestran los tipos de datos fundamentales, así como su tamaño en bytes y rango de valores que es posible representar con ellos.', NULL, 1012, 1052)
INSERT [dbo].[Temas] ([Id], [Texto], [Videos], [CursoId], [IdTemaContent]) VALUES (1054, N'Los tipos de datos int, char, double y bool son los más utilizados en programas simples. Cuando se declaran las variables, estas pueden tambien inicializarse, lo que significa asignar un valor inicial. Buena práctica de programación: Es recomendable inicializar siempre las variables antes de utilizarlas.', NULL, 1012, 1053)
SET IDENTITY_INSERT [dbo].[Temas] OFF
GO
SET IDENTITY_INSERT [dbo].[TemaSeccion] ON 

INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (16, 9, 1, N'Introducción')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (17, 10, 1, N'Introducción')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (18, 10, 2, N'Desarrollo')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (19, 10, 3, N'Evaluación')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (1020, 1011, 1, N'Metodología de la Programación')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (1021, 1011, 2, N'Diagramas de Flujo')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (1022, 1011, 3, N'Introducción a PseInt')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (1023, 1012, 1, N'Introducción')
INSERT [dbo].[TemaSeccion] ([Id], [IdCurso], [IdSeccion], [TemaSeccions]) VALUES (1024, 1012, 2, N'Desarrollo')
SET IDENTITY_INSERT [dbo].[TemaSeccion] OFF
GO
SET IDENTITY_INSERT [dbo].[TomarCurso] ON 

INSERT [dbo].[TomarCurso] ([Id], [IdUsuario], [IdCurso]) VALUES (1031, 2, 1011)
INSERT [dbo].[TomarCurso] ([Id], [IdUsuario], [IdCurso]) VALUES (1032, 2, 1012)
INSERT [dbo].[TomarCurso] ([Id], [IdUsuario], [IdCurso]) VALUES (1034, 2, 10)
SET IDENTITY_INSERT [dbo].[TomarCurso] OFF
GO
