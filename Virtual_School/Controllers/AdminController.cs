﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Virtual_School.DB;
using Virtual_School.Models;

namespace Virtual_School.Controllers
{
    public class AdminController : Controller
    {
        private SchoolContext context;
        private IConfiguration configuration;
        public AdminController(SchoolContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }
        [HttpGet]
        public IActionResult Index()
        {           
            return View();
        }
        [HttpPost]
        public ActionResult Create(Curso curso) // POST
        {
            curso.Fecha = DateTime.Now;

            if (ModelState.IsValid)
            {
                context.Cursos.Add(curso);
                context.SaveChanges(); 

                return RedirectToAction("CreateContenido", new { id = curso.Id });
            }
            return View("Index");
        }
        [HttpGet]

        public ActionResult CreateContenido(int id) // POST
        {
            var curso = context.Cursos.FirstOrDefault(o => o.Id == id);
            return View(curso);
        }
        [HttpPost]
        public ActionResult CreateContenido(ContenidoCurso contCurso) // POST
        {
            if (ModelState.IsValid)
            {
                context.Contenidos.Add(new ContenidoCurso 
                { 
                    IdCurso = contCurso.IdCurso, 
                    Aprender = contCurso.Aprender, 
                    Contenido = contCurso.Contenido, 
                    Requisitos = contCurso.Requisitos,
                    Descripción = contCurso.Descripción, 
                    ParaQuienEs = contCurso.ParaQuienEs
                
                });
                context.SaveChanges();                

                return RedirectToAction("CreateTemaSeccion", new { id = contCurso.IdCurso });
            }

            return View();
        }
        [HttpGet]
        public ActionResult CreateTemaSeccion(int id) // POST
        {
            var curso = context.Cursos.FirstOrDefault(o => o.Id == id);
            ViewBag.secc = context.Seccions;
            ViewBag.temasec = context.temaSeccions;
            ViewBag.temacont = context.TemaContenidos.Where(o => o.IdCurso == id).ToList();
            return View(curso);
        }
        [HttpPost]
        public ActionResult CreateTemaSeccion(TemaSeccion temasec, Seccion secc) // POST
        {

            if (ModelState.IsValid)
            {
                context.temaSeccions.Add(new TemaSeccion
                {
                    IdCurso = temasec.IdCurso,
                    IdSeccion = temasec.IdSeccion,
                    TemaSeccions = temasec.TemaSeccions
                });
                context.SaveChanges();

                return RedirectToAction("CreateTemaSeccion", new { id = temasec.IdCurso });
            }

            return View();
        }
        [HttpPost]
        public ActionResult CreateTemaContenido(TemaContenido temacon, TemaSeccion temasec) // POST
        {
            if (ModelState.IsValid)
            {
                context.TemaContenidos.Add(new TemaContenido
                {
                    IdSecciones = temacon.IdSecciones,
                    IdCurso = temacon.IdCurso,
                    IdTemaSeccion = temasec.IdCurso,
                    TemaContent = temacon.TemaContent
                }); ;
                context.SaveChanges();

                var temaCurso = context.TemaContenidos.Where(o => o.IdTemaSeccion == temasec.IdCurso).ToList();
                var curso = context.Cursos.Where(o => o.Id == temasec.IdCurso).FirstOrDefault();
                int cantidadTemas = temaCurso.Count();
                curso.CantidadTemas = cantidadTemas;
                context.Cursos.Update(curso);
                context.SaveChanges();

                return RedirectToAction("CreateTemaSeccion", new { id = temasec.IdCurso });
            }

            return View();
        }
        public ActionResult CreateTema(Temas tema) // POST
        {
            if (ModelState.IsValid)
            {
                context.Temas.Add(new Temas
                {
                    IdTemaContent = tema.IdTemaContent,
                    CursoId = tema.CursoId,
                    Videos = tema.Videos,
                    Texto = tema.Texto
                });
                context.SaveChanges();

                return RedirectToAction("CreateTemaSeccion", new { id = tema.CursoId });
            }

            return View();
        }

    }
}
