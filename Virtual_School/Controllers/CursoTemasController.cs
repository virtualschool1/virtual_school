﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Virtual_School.DB;
using Virtual_School.Models;

namespace Virtual_School.Controllers
{
    public class CursoTemasController : Controller
    {
        public class jsClass
        {
            public Temas temas { get; set; }

        }
        private SchoolContext _context;
        public CursoTemasController(SchoolContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Authorize]
        public IActionResult Index(int id)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Accounts.First(o => o.Usuario == username);

            ViewBag.Seccion = _context.temaSeccions.Include("Cursos")
                .Include("Seccions")
                .Where(o => o.IdCurso == id).ToList();

            ViewBag.TemaContent = _context.TemaContenidos.Include("Secciones")
                .Where(o => o.IdTemaSeccion == id).ToList();

            ViewBag.Progresos = _context.Progesos.Where(o => o.IdCurso == id && o.IdUser == user.Id).ToList();

            ///barra
            
            var curso = _context.Cursos.Where(o => o.Id == id).FirstOrDefault();
            var progreso = _context.Progesos.Where(o => o.IdCurso == id && o.IdUser == user.Id && o.progress).ToList();

            ViewBag.Progreso = (progreso.Count() * 100) / curso.CantidadTemas;

            return View();
        }
        [HttpGet]
        public IActionResult Video(int idTemaContenido,int idCurso, int idCommenTema)
        {
            var video = _context.Temas.Where(o => o.IdTemaContent == idTemaContenido && o.CursoId == idCurso).FirstOrDefault();
            var co = _context.ComentarioTemas.Include("Accounts").Where(o => o.IdTemaContent == idTemaContenido).ToList();
            var rep = _context.ReplyComents.Where(o => o.ComentTId == 2);
          

            return Json(new { video = video, co = co, rep = rep});
        }

        [HttpPost]
        public IActionResult AddComentario(string detalle, Curso cur, ComentarioTema commet)
        {

            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Accounts.First(o => o.Usuario == username);
            var time = DateTime.Now;


            if (ModelState.IsValid) // no hay mensajes => 0 mensaje
            {
                _context.ComentarioTemas.Add(new ComentarioTema {
                    IdUser = user.Id, 
                    IdTemaContent = commet.IdTemaContent, 
                    Fecha = time, 
                    ComentTema = detalle
                });
                _context.SaveChanges();
                return RedirectToAction("Index", new { id = cur.Id});
            }
            
            return RedirectToAction("Index", new { id = cur.Id });

        }


        [HttpPost]
        public IActionResult ReplyComment(string detalle, int idc, Curso cur)
        {

            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Accounts.First(o => o.Usuario == username);
            var time = DateTime.Now;
            if (ModelState.IsValid) // no hay mensajes => 0 mensaje
            {
                var comentario = _context.ComentarioTemas.Where(o => o.Id == idc).FirstOrDefault();
                comentario.ReplyComment = detalle;
                _context.ComentarioTemas.Update(comentario);


                _context.SaveChanges();
                return RedirectToAction("Index", new { id = cur.Id });
            }
            else
            {
                return RedirectToAction("Index", new { id = cur.Id });
            }

        }

        public IActionResult AñadirProgreso(int id, int cur)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Accounts.First(o => o.Usuario == username);

            var progreso = _context.Progesos.Where(o => o.IdUser == user.Id && o.IdTemaCon == id).FirstOrDefault();

            progreso.progress = true;

            _context.Progesos.Update(progreso);
            _context.SaveChanges();

            return RedirectToAction("Index", new { id = cur });
        }


    }
}
 