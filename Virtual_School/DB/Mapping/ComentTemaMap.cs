﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Virtual_School.Models;

namespace Virtual_School.DB.Mapping
{
    public class ComentTemaMap : IEntityTypeConfiguration<ComentarioTema>
    {
        public void Configure(EntityTypeBuilder<ComentarioTema> builder)
        {
            builder.ToTable("ComentTema");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Accounts)
                .WithMany()
                .HasForeignKey(o => o.IdUser);
        }
    }
}
