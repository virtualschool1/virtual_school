﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Virtual_School.Models
{
    public class ComentarioTema
    {
        public int Id { get; set; }
        public int IdTemaContent { get; set; }
        public int IdUser { get; set; }
        public string ComentTema { get; set; }
        public string ReplyComment { get; set; }
        public DateTime Fecha { get; set; }
        public Account Accounts { get; set; }
    }
}
