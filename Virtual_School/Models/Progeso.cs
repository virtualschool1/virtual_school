﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Virtual_School.Models
{
    public class Progeso
    {
        public int Id { get; set; }
        public int IdCurso { get; set; }
        public int IdUser { get; set; }
        public bool progress { get; set; }
        public int IdTemaCon { get; set; }
        //public int IdSeccion { get; set; }
    }
}
