﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Virtual_School.Models
{
    public class ReplyComent
    {
        public int Id { get; set; }
        public int ComentTId { get; set; }
        public int UserId { get; set; }
        public string Reply { get; set; }
        public DateTime Fecha { get; set; }
    }
}
