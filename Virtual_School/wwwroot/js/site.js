﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

function video(idTemaContenido, idCurso, tr, idCommenTema) {


    var borrar_sl = $('.si-seleccionado');

    for (var i = 0; i < borrar_sl.length; i++) {
        var eles = $(borrar_sl)[i];
        $(eles).attr('class', 'no-seleccionado');
        console.log(eles);

    }



    

    var $videoSeccion = $('div#video');

    $('.pre').css('display', 'block');
    $('.prebg').css('display', 'block');

    $.ajax({
        'url': '/CursoTemas/Video?idTemaContenido=' + idTemaContenido + '&idCurso=' + idCurso,
        'type': 'get'
    }).done(function (items) {
        console.log(items);
        var options = `<iframe muted paused class="video embed-responsive-item" width="730" height="430" src="${items.video.videos}" ></iframe>`
        var options1 = `<textarea style="background-color: white" class="p-3 pl-3" rows="20" cols="95" disabled="disabled" height="430" width="730" required>${items.video.texto}</textarea>`
        if (items.video.videos != null) {
            $videoSeccion.html(options);
        }
        else {
            $videoSeccion.html(options1);
        }
        


        if (items.co.length > 0) {

            var themes = '';
            items.co.map((e, a, i) => {                
                console.log(e);

                themes +=
                    `<div class="media-body mr-5 mb-3">
                        <div class="well well-lg">
                            <h4 class="media-heading text-uppercase reviews text-light">${e.accounts.usuario} <i class="fas fa-user fa-lg"></i></h4>
                            <ul class="media-date text-uppercase reviews list-inline">
                                <li class="dd">${e.fecha}</li>
                            </ul>
                            <p class="media-comment pt-3">${e.comentTema}</p>
                            
                            </div > 
                            <div class="ml-5">
                            <h4 class="media-heading text-uppercase reviews text-light" >${e.accounts.usuario} <i class="fas fa-user fa-lg"></i></h4 >
                            <ul class="media-date text-uppercase reviews list-inline">
                                <li class="dd">${e.fecha}</li>
                           </ul>
                           <p class="media-comment pt-3">${e.replyComment}</p> 
                            </div>
                            <div class="commentar">
                            <br />
                            <form method="post" action="/cursotemas/ReplyComment?id=${idCurso}&idc=${e.id}">
                                <input type="hidden" name="ComentTId" value="${idTemaContenido}" />
                                <div class="form-group coment">
                                <textarea name="detalle" required></textarea>
                                <br />
                                <button class="respuesta btn btn-info btn-circle text-uppercase" type="submit"><i class="reply fas fa-reply"></i> Responder</button>
                                </div>
                            </form>
                        </div>
                    </div>`
            });

            $('#comments').html(themes);
            $('#comments').append(`<h5 class="dudas text-light pt-2" > Añade un comentario: </h5 >
                    <br />
                    <form method="post" action="/cursotemas/AddComentario?id=${idCurso}">
                        <input type="hidden" name="IdTemaContent" value="${idTemaContenido}" />
                        <div class="form-group coment">
                            <textarea class="form-control" rows="10" name="detalle" required></textarea>
                            <br />
                            <button class="BtnLike btn btn-primary" type="submit">Enviar</button>
                        </div>
                    </form>`);

        } else {
            $('#comments').html(
                `<h5 class="dudas text-light pt-2">No hay comentarios añade uno: </h5>
                <br />
                <form method="post" action="/cursotemas/AddComentario?id=${idCurso}">
                    <input type="hidden" name="IdTemaContent" value="${idTemaContenido}" />
                    <div class="form-group coment">
                        <textarea class="form-control" rows="10" name="detalle" required></textarea>
                        <br />
                        <button class="BtnLike btn btn-primary" type="submit">Enviar</button>
                    </div>
                </form>`
            );

        }
            


        setTimeout(() => {
            $('.pre').css('display', 'none');
            $('.prebg').css('display', 'none');
            $(tr).addClass('si-seleccionado');
        }, 1500);


    })   

}

//<h4 class="media-heading text-uppercase reviews text-light" > ${e.accounts.usuario} <i class="fas fa-user fa-lg"></i></h4 >
//                            <ul class="media-date text-uppercase reviews list-inline">
//                                <li class="dd">${e.fecha}</li>
//                            </ul>
//                            <p class="media-comment pt-3">${e.Reply}</p> 

// Write your JavaScript code.
